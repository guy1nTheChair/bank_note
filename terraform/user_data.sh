#!/bin/bash
yum upgrade -y
yum install docker -y
#sudo yum install git -y
usermod -aG docker ec2-user
service docker start
docker pull registry.gitlab.com/guy1nthechair/bank_note:latest
docker run -d -p 5000:5000 registry.gitlab.com/guy1nthechair/bank_note:latest
